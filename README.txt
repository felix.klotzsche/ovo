Readme for OpenVirtualObject (OVO) database per folder

*** Data ***

obj_output_all.csv 		means per object - all participants
obj_output_young.csv 		means per object - younger participants
obj_output_old.csv 		means per object - older participants

*** Analyses ***
**OVO/dataframes

raw data.txt 		all data - all participants
data_all.csv 		clean data - all participants
data_all_younger.csv 	clean data - younger participants
data_all_older.csv 	clean data - older participants
objMeans_all.csv 	means per dimension per object - all participants
objMeans_young.csv 	means per dimension per object - younger participants
objMeans_old.csv 	means per dimension per object - older participants
obj_output_all.csv 	means/mdist/modal name/Hstat/NA%  - all participants
obj_output_young.csv 	means/mdist/modal name/Hstat/NA% - younger participants
obj_output_old.csv 	means/mdist/modal name/Hstat/NA% - older participants
data_all_names.txt      naming data
     

**OVO/scripts

* 0V0_prep_data.R
This script creates the dataframes that are used in the analysis (see OVO/dataframes) 
			
* OVO_analysis.Rmd
This script performs the following analyses:
1. Dimensions: descriptive statistics / boxplot
2. Dimensions: Shapiro-Wilk test
3. Dimensions: correlation analysis
4. Dimensions: multivariate outlier detection
5: Dimension: Database comparison
6: Create obj_output dataframes

* OVO_analysis_semcat.Rmd  
This script performs the semantic category analysis 

* OVO_analysis_naming.Rmd    
This script performs the naming data analyses:


**OVO/naming_data_analyses
This folder contains some resources needed to analyse the naming data. 

**OVO/figures
This folder contains all figures that are included in the manuscript:
            Fig. 1 Example objects
            Fig. 2 Boxplot


